package com.roque.springcloud.producer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.roque.springcloud.producer.domain.EventType;
import com.roque.springcloud.producer.domain.LibraryEvent;
import com.roque.springcloud.producer.producer.LibraryEventProducer;

@RestController
public class LibraryEventController {
	
	@Autowired
	LibraryEventProducer producer;

	@PostMapping("/v1/libraryevent")
	public ResponseEntity<LibraryEvent> post(@RequestBody LibraryEvent event) throws JsonProcessingException{
		event.setType(EventType.NEW);
		producer.send(event);
		return ResponseEntity.status(HttpStatus.CREATED).body(event);
		
	}
	
	@PutMapping("/v1/libraryevent")
	public ResponseEntity<?> put(@RequestBody LibraryEvent event) throws JsonProcessingException{
		
		if(null == event.getId()){
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Pass the id");
		}
		
		event.setType(EventType.UPDATE);
		producer.send(event);
		return ResponseEntity.status(HttpStatus.OK).body(event);
		
	}
	
}
