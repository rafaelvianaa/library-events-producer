package com.roque.springcloud.producer.integration.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.roque.springcloud.producer.domain.Book;
import com.roque.springcloud.producer.domain.EventType;
import com.roque.springcloud.producer.domain.LibraryEvent;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LibraryEventControllerIntegrationTest {
	
	@Autowired
	TestRestTemplate restTemplate;
	
	@Test
	void postLibraryEvent() {
		
		Book book = Book.builder()
				.author("Rafael")
				.name("Test name")
				.id(1)
				.build();
		
		LibraryEvent event = LibraryEvent.builder()
		.book(book)
		.id(null)
		.type(EventType.NEW)
		.build();
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("content-type", MediaType.APPLICATION_JSON_VALUE.toString());
		
		HttpEntity<LibraryEvent> request = new HttpEntity<>(event , headers);
		
		ResponseEntity<LibraryEvent> response = restTemplate.exchange("/v1/libraryevent", HttpMethod.POST , request , LibraryEvent.class);
		
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		
		
	}

}
